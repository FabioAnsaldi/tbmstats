# TbmStats #

It is an implementation of a stats generator

For more informations [read](http://jira.service.triboo.it/browse/LEOFORM-268) this:
http://jira.service.triboo.it/browse/JADV-682

### Installation and configuration ###

To install all required modules  (if you have project folder) go into project folder and then run:

```sh
npm install
```
######

To show Demo in your favorite browser, in the command line run:

```sh
npm run start
```
> This command runs Web server, source DB server and stats DB server*

######

To start Mocha test server in the command line run:

```sh
npm run test
```
######

To build the project run the command:

```sh
npm run build
```
> This command runs gulp behavius. It Generates the minify file in build folder and JSON string file in the same

######

To create only JSON file you can run the command:

```sh
npm run json
```

###### License ######

[Triboo SPA - ALL RIGHTS RESERVED](http://www.triboomedia.it/privacy-policy/)