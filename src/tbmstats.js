let program = require( 'commander' );
let request = require( 'request' );
let jsonfile = require( 'jsonfile' );

// Set parameters manual, showable appending --help or -h
program
    .version( '1.0.0' )
    .option( '--config [path/filename.json]', 'set the configuration envirorment with the specific [path/filename.json] file' )
    .option( '--env [dev/prod]', 'set process environment with the [dev/prod] config file in config/ directory' )
    .parse( process.argv );

process.env.NODE_ENV = program.env || 'dev';
let configPath = program.config || './config/' + process.env.NODE_ENV + '.json';

let readConfigFile = () => {
    return new Promise( ( fulfill, reject ) => {
        // Try to read configuration file in config/ directory
        jsonfile.readFile( configPath, ( err, obj ) => {
            if ( err ) {
                let message = 'Config file not found. Error code: ';
                return reject( new Error( message + err.errno + '. Path: ' + err.path ) );
            }
            else {
                return fulfill( obj );
            }
        } );
    } );
};

let checkJsonConfig = ( config ) => {
    return new Promise( ( fulfill, reject ) => {
        // Check if we have configuration environment
        if ( !config || typeof config !== 'object' ) {
            let message = 'Config file isn\'t a valid JSON file: It is a ';
            return reject( new Error( message + (typeof config) ) );
        } else {
            return fulfill( config );
        }
    } );
};

let makeResourceRequest = ( config ) => {
    let resourceServer = config.resource.protocol + '://' + config.resource.host + ':' + config.resource.port + '/' + config.resource.database;
    return new Promise( ( fulfill, reject ) => {
        // try to make resource request at the database server
        let req = request
            .get( resourceServer )
            .on( 'response', ( res ) => {
                return fulfill( { config: config, request: req, response: res } );
            } )
            .on( 'error', ( err ) => {
                let message = 'Resource request error! Error code: ';
                return reject( new Error( message + err.errno + '. address: ' + err.address + '. port: ' + err.port ) );
            } );
    } )
};

let checkStatusCode = ( obj ) => {
    return new Promise( ( fulfill, reject ) => {
        // Check if we have any valid response from the resource database server
        if ( obj.response.statusCode !== 200 || obj.response.headers[ 'content-type' ] !== 'application/json; charset=utf-8' ) {
            let message = 'Response isn\'t a valid or status code isn\'t 200. statusCode: ';
            return reject( new Error( message + obj.response.statusCode + '. content-type: ' + obj.response.headers[ 'content-type' ] ) );
        } else {
            return fulfill( { config: obj.config, request: obj.request, response: obj.response } );
        }
    } );
};

let makeStoringRequest = ( obj ) => {
    let storingServer = obj.config.destination.protocol + '://' + obj.config.destination.host + ':' + obj.config.destination.port + '/' + obj.config.destination.database;
    return new Promise( ( fulfill, reject ) => {
        // try to make storing request at the database server
        let req = request.put( storingServer )
            .on( 'response', ( res ) => {
                return fulfill( { config: obj.config, request: req, response: res } );
            } )
            .on( 'error', ( err ) => {
                let message = 'Storing request error! Error code: ';
                return reject( new Error( message + err.errno + '. address: ' + err.address + '. port: ' + err.port ) );
            } );
        obj.request.pipe( req );
    } );
};

let echoResponse = ( e ) => {
    console.dir( e );
    let d = new Date();
    let t = d.getTime();

    console.log( t );
};

readConfigFile().then( checkJsonConfig ).then( makeResourceRequest ).then( checkStatusCode ).then( makeStoringRequest ).catch( echoResponse );
